class Dish < ApplicationRecord
  belongs_to :restaurant
  has_one_attached :picture
validates :picture,  file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

end
